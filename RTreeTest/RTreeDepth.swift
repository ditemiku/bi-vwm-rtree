enum RTreeError: ErrorType {
    case NotBalanced
}


extension RTreeNode {
    func validateDepth() throws -> UInt {
        var sameDepth: UInt?
        for (_, node) in children.enumerate() {
            let childDepth = try node.validateDepth()
            guard let _ = sameDepth else {
                sameDepth = childDepth
                continue
            }
            
            if childDepth != sameDepth {
                throw RTreeError.NotBalanced
            }
        }
        
        guard let depth = sameDepth else {
            return 0 // leaf
        }
        return 1 + depth
    }
}
