import XCTest

class RTreeDiskSwapTest: XCTestCase {
    func testSimple() {
        var tree = RTree(params: Params(min: 2, max: 4, dimensions: 2, diskPagesPath: "/tmp/rtree-test", keepToDepth: 0))

        let box = Box(x1: 10, x2: 20, y1: 50, y2: 70)
        tree.insert(box)
        
        let biggerBox = Box(x1: 5, x2: 25, y1: 40, y2: 80)
        tree.insert(biggerBox)
        
        let smallerBox = Box(x1: 12, x2: 18, y1: 55, y2: 65)
        tree.insert(smallerBox)
        
        let sideBox = Box(x1: 2, x2: 30, y1: 58, y2: 62)
        tree.insert(sideBox)
        
        tree.root.purge()
        XCTAssert(!tree.root.children.isLoaded)
        XCTAssert(tree.root.children.count == 4)

        tree._debug()
        let result = tree.search(Box(x1: 15, x2: 15, y1: 60, y2: 60))
        
        XCTAssert(tree.root.children.isLoaded)
        XCTAssert(tree.root.children.count == 4)

        XCTAssert(result.count == 4)
    }
    
    func testPerformance_Random_QuadraticSplit() {
        var run = 0
        self.measureBlock {
            var tree = RTree(params: Params(min: 20, max: 50, dimensions: 2, diskPagesPath: "/tmp/rtree-perf-\(run)", keepToDepth: 0))
            srandom(1) // intentionally static salt for repeatable runs
    
            for _ in 0..<500 {
                tree.insert(Box.initRandomInsert())
            }
            
            run += 1
        }
    }

}
