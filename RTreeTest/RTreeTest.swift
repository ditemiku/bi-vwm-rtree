import XCTest

class RTreeTest: XCTestCase {
    func testSimple() {
        var tree = RTree(params: Params(min: 2, max: 4, dimensions: 2))
        
        XCTAssert(tree.root.children.isEmpty)
        let box = Box(x1: 10, x2: 20, y1: 50, y2: 70)
        tree.insert(box)
        XCTAssert(tree.root.boundingBox == box, "inserting box must update parent bounding box")
        XCTAssert(!tree.root.children.isEmpty)

        let biggerBox = Box(x1: 5, x2: 25, y1: 40, y2: 80)
        tree.insert(biggerBox)
        XCTAssert(tree.root.boundingBox == biggerBox, "inserting box must update parent bounding box")
        
        let smallerBox = Box(x1: 12, x2: 18, y1: 55, y2: 65)
        tree.insert(smallerBox)
        XCTAssert(tree.root.boundingBox == biggerBox, "smaller box must not make bounding box lower")
        
        let sideBox = Box(x1: 2, x2: 30, y1: 58, y2: 62)
        tree.insert(sideBox)
        
        tree._debug()
        let result = tree.search(Box(x1: 2, x2: 2, y1: 60, y2: 60));
        XCTAssert(result.count == 1)
        XCTAssert(result[0] == sideBox)
    }

    func testSplittingInsert() {
        var tree = RTree(params: Params(min: 2, max: 5, dimensions: 2))
        for n in 0...128 {
            tree.insert(Box(x1: n, x2: n + 10, y1: n, y2: n + 10))
        }
       
        do {
            try tree.root.validateDepth()
        } catch {
            XCTAssert(false, "tree must be balanced")
        }
        XCTAssert(true, "tree is balanced")
    }

    func testPerformance_LinearOverlap_QuadraticSplit_insert() {
        // performs worse then random distribution
        self.measureBlock {
            var tree = RTree(params: Params(min: 20, max: 50, dimensions: 2))
            
            for n in 0...50_000 {
                tree.insert(Box(x1: n, x2: n + 10, y1: n, y2: n + 10))
            }
        }
    }

    func testPerformance_LinearOverlap_QuadraticSplit_search() {
        // performs worse then random distribution
        var tree = RTree(params: Params(min: 20, max: 50, dimensions: 2))
        
        for n in 0...50_000 {
            tree.insert(Box(x1: n, x2: n + 10, y1: n, y2: n + 10))
        }
        
        self.measureBlock {
            for n in 0...10_000_000 {
                tree.search(Box(x1: n, x2: n + 10, y1: n, y2: n + 10))
            }
        }
    }

    
    func testPerformance_Random_QuadraticSplit_insert() {
        self.measureBlock {
            var tree = RTree(params: Params(min: 4, max: 8, dimensions: 2))
            srandom(1) // intentionally static salt for repeatable runs
            
            for _ in 0...50_000 {
                tree.insert(Box.initRandomInsert(400))
                tree._debug();
            }
        }
    }

    func testPerformance_Random_QuadraticSplit_search() {
        var tree = RTree(params: Params(min: 20, max: 50, dimensions: 2))
        srandom(1) // intentionally static salt for repeatable runs
        
        for _ in 0..<100_000 {
            tree.insert(Box.initRandomInsert())
        }
        
        self.measureBlock {
            for _ in 0..<10_000_000 {
                // test only small ranges, large ranges are not optimized
                let x = random()
                let y = random()
                let size = random() % 10
                tree.search(Box(x1: x, x2: x + size, y1: y, y2: y + size))
            }
        }
    }
    
}