class RTreeTest: XCTestCase {
  func testBoundingBoxUpdates() {
    var tree = RTree(params: Params(min: 2, max: 4, dimensions: 2))

    XCTAssert(tree.root.children.isEmpty)
    let box = Box(x1: 10, x2: 20, y1: 50, y2: 70)
    tree.insert(box)
    XCTAssert(tree.root.boundingBox == box,
    	"inserting box must update parent bounding box")
    XCTAssert(!tree.root.children.isEmpty)
  }
}
