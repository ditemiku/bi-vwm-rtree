import Foundation

struct Samples: CustomStringConvertible {
    var times: [CFAbsoluteTime] = []

    var average: CFAbsoluteTime {
        get {
            return times.reduce(0, combine: +) / Double(times.count)
        }
    }
    
    var stddev: Double {
        let sum = times.reduce(0) { $0 + $1}
        let sum2 = times.reduce(0) { $0 + $1*$1 }
        let n = Double(times.count)

        return sqrt( sum2 / n - sum * sum / n / n)
    }
    
    mutating func addSample(time: CFAbsoluteTime) {
        times.append(time)
    }
    
    var description: String {
        get {
            let fmtAvg = String(format: "%.3f", average)
//            let fmtSd = String(format: "%.3f", stddev * 100)
//            return "\(fmtAvg) [SD=\(fmtSd)%]"
            let absSd = average * stddev
            let fmtAbsSd = String(format: "%.3f", absSd)
            return "\(fmtAvg)\t\(fmtAbsSd)"
        }
    }
}
