import Foundation

struct Timer {
    let start: CFAbsoluteTime
    init() {
        start = CFAbsoluteTimeGetCurrent()
    }
    func elapsed() -> CFAbsoluteTime {
        return CFAbsoluteTimeGetCurrent() - start
    }
}

func sample(runs: UInt = 10, test: () -> Void) -> Samples {
    var samples = Samples()
    for _ in 1...runs {
        shell("rm", "-rf", "/tmp/rtree-mem-swap")
        shell("mkdir", "/tmp/rtree-mem-swap")
        shell("touch", "/tmp/removed")

        let timer = Timer()
        test()
        samples.addSample(timer.elapsed())
    }
    return samples
}

func sample(tree: RTree, runs: UInt = 10, test: (tree: RTree) -> Void) -> Samples {
    var samples = Samples()
    for _ in 1...runs {
        shell("rm", "-rf", "/tmp/rtree-mem-swap")
        shell("mkdir", "/tmp/rtree-mem-swap")
        shell("touch", "/tmp/removed")

        let timer = Timer()
        test(tree: tree)
        samples.addSample(timer.elapsed())
    }
    return samples
}

func shell(args: String...) -> Int32 {
    let task = NSTask()
    task.launchPath = "/usr/bin/env"
    task.arguments = args
    task.launch()
    task.waitUntilExit()
    return task.terminationStatus
}

func report_memory() -> mach_vm_size_t? {
    // constant
    let MACH_TASK_BASIC_INFO_COUNT = (sizeof(mach_task_basic_info_data_t) / sizeof(natural_t))

    // prepare parameters
    let name   = mach_task_self_
    let flavor = task_flavor_t(MACH_TASK_BASIC_INFO)
    var size   = mach_msg_type_number_t(MACH_TASK_BASIC_INFO_COUNT)

    // allocate pointer to mach_task_basic_info
    let infoPointer = UnsafeMutablePointer<mach_task_basic_info>.alloc(1)

    // call task_info - note extra UnsafeMutablePointer(...) call
    let kerr = task_info(name, flavor, UnsafeMutablePointer(infoPointer), &size)

    // get mach_task_basic_info struct out of pointer
    let info = infoPointer.move()

    // deallocate pointer
    infoPointer.dealloc(1)

    // check return value for success / failure
    if kerr == KERN_SUCCESS {
        return info.resident_size;
    } else {
        return nil;
    }
}




//print("Insert performance over node size")
//print("maxSize\tspeed\tsd")
//for var maxSize = 4; maxSize < 50; maxSize += 2 {
//    let speed = sample { () -> Void in
//        srandom(1)
//        var tree = RTree(params: Params(min: maxSize / 2, max: maxSize, dimensions: 2))
//
//        for _ in 1...100_000 {
//            tree.insert(Box.initRandomInsert())
//        }
//    }
//    print("\(maxSize)\t\(speed)")
//}

let bestMaxSize = 10
print("Best insert performance is for nodeCount=\(bestMaxSize)")



//print("Insert performance over min size")
//print("minSize\tspeed\tsd")
//for minSize in 2...bestMaxSize-1 {
//    let speed = sample { () -> Void in
//        srandom(1)
//        var tree = RTree(params: Params(min: minSize, max: bestMaxSize, dimensions: 2))
//
//        for _ in 1...100_000 {
//            tree.insert(Box.initRandomInsert())
//        }
//    }
//    print("\(minSize)\t\(speed)")
//}

let bestMinSize = 5
print("Best insert performance is for min size=\(bestMinSize)")


print("Search performance over node size 1m-in-10k")
print("maxSize\tspeed\tsd")
// search-maxsize-speed-1m-in-10k.tsv
//for var maxSize = 4; maxSize < 25; maxSize += 2 {
//    srandom(1)
//    var tree = RTree(params: Params(min: maxSize / 2, max: maxSize, dimensions: 2))
//
//    for _ in 1...10_000 {
//        tree.insert(Box.initRandomInsert())
//    }
//
//    let speed = sample(tree, test: { (tree: RTree) in
//        srandom(1)
//        for _ in 1...1_000_000 {
//            // test only small ranges, large ranges are not optimized
//            let x = random()
//            let y = random()
//            let size = random() % 10
//            tree.search(Box(x1: x, x2: x + size, y1: y, y2: y + size))
//        }
//    })
//    print("\(maxSize)\t\(speed)")
//}

print("Search performance over node size 1m-in-100k")
print("maxSize\tspeed\tsd")
// search-maxsize-speed-1m-in-100k.tsv
//for var maxSize = 4; maxSize < 25; maxSize += 2 {
//    srandom(1)
//    var tree = RTree(params: Params(min: maxSize / 2, max: maxSize, dimensions: 2))
//
//    for _ in 1...100_000 {
//        tree.insert(Box.initRandomInsert())
//    }
//
//    let speed = sample(tree, test: { (tree: RTree) in
//        srandom(1)
//        for _ in 1...1_000_000 {
//            // test only small ranges, large ranges are not optimized
//            let x = random()
//            let y = random()
//            let size = random() % 10
//            tree.search(Box(x1: x, x2: x + size, y1: y, y2: y + size))
//        }
//    })
//    print("\(maxSize)\t\(speed)")
//}
//
//
//print("Search performance over node size 1m-in-100k-small-range")
//print("maxSize\tspeed\tsd")
//// search-maxsize-speed-1m-in-100k-small-range.tsv
//for var maxSize = 4; maxSize < 50; maxSize += 2 {
//    srandom(1)
//    var tree = RTree(params: Params(min: maxSize / 2, max: maxSize, dimensions: 2))
//
//    for _ in 1...10_000 {
//        tree.insert(Box.initRandomInsert())
//    }
//
//    let speed = sample(tree, test: { (tree: RTree) in
//        srandom(1)
//        for _ in 1...1_000 {
//            let result = tree.search(Box.initRandomInsert())
//            print(result.count)
//        }
//    })
//    print("\(maxSize)\t\(speed)")
//}

//
//print("Search performance over node size 1m-in-100k-medium-range")
//print("maxSize\tspeed\tsd")
//// search-maxsize-speed-1m-in-100k-medium-range.tsv
//for var maxSize = 4; maxSize < 50; maxSize += 2 {
//    srandom(1)
//    var tree = RTree(params: Params(min: maxSize / 2, max: maxSize, dimensions: 2))
//
//    for _ in 1...10_000 {
//        tree.insert(Box.initRandomInsert())
//    }
//
//    let speed = sample(tree, test: { (tree: RTree) in
//        srandom(1)
//        for _ in 1...1_000 {
//            let result = tree.search(Box.initRandomInsert(8192))
//            print(result.count)
//        }
//    })
//    print("\(maxSize)\t\(speed)")
//}
//
//print("Search performance over node size 10k-in-1k-large-range")
//print("maxSize\tspeed\tsd")
//// search-maxsize-speed-1m-in-100k-large-range.tsv
//for var maxSize = 4; maxSize < 50; maxSize += 2 {
//    srandom(1)
//    var tree = RTree(params: Params(min: maxSize / 2, max: maxSize, dimensions: 2))
//
//    for _ in 1...1_000 {
//        tree.insert(Box.initRandomInsert())
//    }
//
//    let speed = sample(tree, test: { (tree: RTree) in
//        srandom(1)
//        for _ in 1...10_000 {
//            let box = Box.initRandomQuery(size: 64);
//            let result = tree.search(box)
////            print(result.count)
//        }
//    })
//    print("\(maxSize)\t\(speed)")
//}



//print("Memory usage: disk swapping")
//print("depth\tspeed\tsd\tmaxMemory")
//// benchMemSwapSwapDuration.tsv
//for depth in 0..<15 {
//    let benchMemSwap = {
//        srandom(1)
//        var treeDS = RTree(params: Params(min: 5, max: 10, dimensions: 2, diskPagesPath: "/tmp/rtree-mem-swap", keepToDepth: UInt(depth)))
//
//        let baseline = report_memory()!
//        var maxMemoryUsed = baseline;
//        for n in 1..<10_000 {
//            treeDS.insert(Box.initRandomInsert())
//        }
//    }
//    let benchMemSwapSwapDuration = sample(test: benchMemSwap)
//    print("\(depth)\t\(benchMemSwapSwapDuration)")
//}
//
//exit(0)
//
//
//
//
//
//srandom(1)
//var treeInsertMem = RTree(params: Params(min: 2, max: 5, dimensions: 2, diskPagesPath: "/tmp/rtree-insert-mem", keepToDepth: 3))
//
//print("Speed: insert time: in-memory")
//for n in 1..<10_000 {
//    let timer = Timer()
//    treeInsertMem.insert(Box.initRandomInsert())
//
//    if n % 10 == 0 {
//        print("\(n): \(timer.elapsed())")
//    }
//}
//
//
//srandom(1)
//var treeInsertSwap = RTree(params: Params(min: 2, max: 5, dimensions: 2, diskPagesPath: "/tmp/rtree-insert-swap", keepToDepth: 3))
//
//print("Speed: insert time: swapping")
//for n in 1..<10_000 {
//    let timer = Timer()
//    treeInsertSwap.insert(Box.initRandomInsert())
//
//    if n % 10 == 0 {
//        print("\(n): \(timer.elapsed())")
//    }
//}
//


srandom(1)
print("Speed: search in dimensions")
print("dim\tspeed\tsd")
for dim in 2...50 {
    let mod = 65_536
    
    var samples = Samples()
    
    var tree = RTree(params: Params(min: 2, max: 4, dimensions: UInt(dim)))
    for _ in 1..<1_000 {
        var segments: [Segment] = []
        for d in 0..<dim {
            let position = random() % mod
            let size = random() % 64
            segments.append(Segment(min: position, max: position + size))
        }

        let box = Box(segments: segments)
        tree.insert(box)
    }

    for _ in 1...10 {
        let timer = Timer()
        for _ in 1..<5_000 + dim * 1000 {
            var segments: [Segment] = []
            for d in 0..<dim {
                let position = random() % mod
                let size = random() % 64
                segments.append(Segment(min: position, max: position + size))
            }
            
            let box = Box(segments: segments)
            tree.search(box)
        }
        samples.addSample(timer.elapsed())
    }

    print("\(dim)\t\(samples)")
}

