import Foundation

struct Area {
    
    static func compute(a: Box, b: Box) -> Double {
        var area: Double = 0
        
        let maxDim = Int(a.dimensions)
        
        for dim in 0 ..< maxDim {
            area += log2(Double(
                max(a.segments[dim].max, b.segments[dim].max) -
                min(a.segments[dim].min, b.segments[dim].min)))
        }
        
        return area
    }
}
