import Foundation

/**
 * Hyperrectangle
 */
struct Box {
    let dimensions: UInt
    let segments: [Segment]
    
    static func fromData(data: NSData, offset oldOffset: Int) -> (Box, Int) {
        var offset = oldOffset
        var dimensions: Int = 0
        data.getBytes(&dimensions, range: NSMakeRange(offset, sizeof(Int)))
        offset += sizeof(Int)
        
        var segments: [Segment] = []
        for _ in 0..<dimensions {
            var min = 0
            var max = 0
            
            data.getBytes(&min, range: NSMakeRange(offset, sizeof(Int)))
            offset += sizeof(Int)
            
            data.getBytes(&max, range: NSMakeRange(offset, sizeof(Int)))
            offset += sizeof(Int)
            
            segments.append(Segment(min: min, max: max))
        }
        return (Box(segments: segments), offset)
    }
    
    init(segments: [Segment]) {
        self.segments = segments
        dimensions = UInt(segments.count)
    }

    init(x1: Int, x2: Int, y1: Int, y2: Int) {
        self.init(segments: [Segment(x1: x1, x2: x2), Segment(x1: y1, x2: y2)])
    }
    
    var area: Double {
        get {
            // simulate multiplication without int overflow in higher dimensions
            var logs: Double = 0
            for segment in segments {
                logs += log2(Double(segment.max - segment.min))
            }
            return logs
        }
    }
    
    func merge(other: Box?) -> Box {
        guard let other = other else {
            return self
        }

        var merged: [Segment] = segments
        
        let maxDim = merged.count
        for dimension in 0 ..< maxDim {
            let segA = segments[dimension]
            let segB = other.segments[dimension]
            
            merged[dimension] = Segment(
                min: min(segA.min, segB.min),
                max: max(segA.max, segB.max))
        }
        return Box(segments: merged)
    }

    func intersects(inout other: Box) -> Bool {
        for dimension in 0 ..< Int(dimensions) {
            let segA = segments[dimension]
            let segB = other.segments[dimension]
            if segA.min > segB.max || segA.max < segB.min {
                return false
            }
        }
        
        return true
    }
    
    func writeToBytes(data: NSMutableData) {
        var dim = dimensions
        data.appendBytes(&dim, length: sizeof(UInt))
        for segment in segments {
            var min = segment.min
            var max = segment.max
            data.appendBytes(&min, length: sizeof(Int))
            data.appendBytes(&max, length: sizeof(Int))
        }
    }
}

extension Box : Equatable {}

func ==(lhs: Box, rhs: Box) -> Bool {
    if lhs.segments.count != rhs.segments.count {
        return false
    }
    
    let maxN = lhs.segments.count
    for n in 0 ..< maxN {
        if lhs.segments[n] != rhs.segments[n] {
            return false
        }
    }
    return true
}
