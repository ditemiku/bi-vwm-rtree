import Cocoa

extension Box {
//    static func initRandom(mod mod: Int = 1024) -> Box {
//        return Box(
//            x1: random() % mod,
//            x2: random() % mod,
//            y1: random() % mod,
//            y2: random() % mod
//        )
//    }
    
    static func initRandomInsert(mod: Int = 65_536) -> Box {
        let size = random() % 64
        let x = random() % mod
        let y = random() % mod
        return Box(
            x1: x,
            x2: x + size,
            y1: y,
            y2: y + size
        )
    }
    
    static func initRandomQuery(size size: Int = 256, mod: Int = 65_536) -> Box {
        let x = random() % mod
        let y = random() % mod
        return Box(
            x1: x,
            x2: x + size,
            y1: y,
            y2: y + size
        )
    }
}
