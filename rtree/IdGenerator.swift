import Foundation

struct Id {
    let a, b, c: Int
    
    init() {
        self = IdGenerator.get()
    }
    
    init(_ a: Int, _ b: Int, _ c: Int) {
        self.a = a
        self.b = b
        self.c = c
    }
    
    static func fromData(data: NSData, offset oldOffset: Int) -> (Id, Int) {
        var offset = oldOffset
        var a = 0, b = 0, c = 0
        
        data.getBytes(&a, range: NSMakeRange(offset, sizeof(Int)))
        offset += sizeof(Int)
        
        data.getBytes(&b, range: NSMakeRange(offset, sizeof(Int)))
        offset += sizeof(Int)
        
        data.getBytes(&c, range: NSMakeRange(offset, sizeof(Int)))
        offset += sizeof(Int)

        return (Id(a, b, c), offset)
    }

    func writeToBytes(data: NSMutableData) {
        var va = a, vb = b, vc = c
        data.appendBytes(&va, length: sizeof(Int))
        data.appendBytes(&vb, length: sizeof(Int))
        data.appendBytes(&vc, length: sizeof(Int))
    }
}

struct IdGenerator {
    static var partA = 0
    static var partB = 0
    static var partC = 0
    
    static func get() -> Id {
        // gcd(19, 23) == 0
        partA = (partA + 1) % 19
        partB = (partB + 1) % 23
        partC += 1
        return Id(partA, partB, partC)
    }
}
