import Foundation

enum List<Element> {
    case End
    indirect case Node(Element, next: List<Element>)
}

extension List {
    func cons(x: Element) -> List {
        return .Node(x, next: self)
    }
}

extension List {
    mutating func push(x: Element) {
        self = self.cons(x)
    }

    mutating func pop() -> Element? {
        switch self {
        case .End: return nil
        case let .Node(x, next: xs):
            self = xs
            return x
        }
    }
}
