import Foundation

class NodeContainer {
    var params: Params
    var boundingBox: Box? = nil
    private var ids: [Id] = []
    var nodes: [RTreeNode] = []
    private var loaded = true

    init(params: Params) {
        self.params = params
    }
    
    init(params: Params, nodes: [RTreeNode]) {
        self.params = params
        appendContentsOf(nodes)
    }
    
    init(params: Params, data: NSData, offset: Int) {
        self.params = params
        self.loaded = false
        readIdsBytes(data, offset: offset)
    }
    
    var count: Int {
        get {
            if loaded {
                return nodes.count
            } else {
                return ids.count
            }
        }
    }
    
    var isEmpty: Bool {
        get {
            if loaded {
                return nodes.isEmpty
            } else {
                return ids.isEmpty
            }
        }
    }
    
    var isLoaded: Bool {
        get {
            return loaded
        }
    }
    
    var area: Double {
        get {
            guard let boundingBox = boundingBox else {
                return 0
            }
            return boundingBox.area
        }
    }
    
    func enumerate() -> EnumerateSequence<[RTreeNode]> {
        loadNodesFromDisk()
        return nodes.enumerate()
    }
    
    subscript(index: Int) -> RTreeNode {
        get {
            return nodes[index]
        }
    }
    
    func loadNodesFromDisk() {
        if loaded {
            return
        }

        nodes.removeAll()
        for id in ids {
            nodes.append(loadNodeFromDisk(id))
        }
        loaded = true
    }
    
    private func loadNodeFromDisk(id: Id) -> RTreeNode {
        let (_, file) = RTreeNode.getPageFile(id, params: params)
        let data = NSData(contentsOfFile: file)!
        return RTreeNode(params: params, id: id, data: data)
    }
    
    func writeIdsBytes(data: NSMutableData) {
        if isLoaded {
            ids = nodes.map { $0.id }
        }

        var haxBoundingBox = boundingBox != nil
        data.appendBytes(&haxBoundingBox, length: sizeof(Bool))
        if haxBoundingBox {
            boundingBox?.writeToBytes(data)
        }

        var childrenCount = count
        data.appendBytes(&childrenCount, length: sizeof(Int))
        for id in ids {
            id.writeToBytes(data)
        }
    }
    
    private func readIdsBytes(data: NSData, offset oldOffset: Int) {
        var offset = oldOffset
        var hasBoundingBox = false
        var childrenCount = 0

        data.getBytes(&hasBoundingBox, range: NSMakeRange(offset, sizeof(Bool)))
        offset += sizeof(Bool)
        
        if hasBoundingBox {
            var newBox: Box
            (newBox, offset) = Box.fromData(data, offset: offset)
            boundingBox = newBox
        }
        
        data.getBytes(&childrenCount, range: NSMakeRange(offset, sizeof(Int)))
        offset += sizeof(Int)
        
        loaded = false
        ids = []

        for _ in 0..<childrenCount {
            var id: Id
            (id, offset) = Id.fromData(data, offset: offset)
            ids.append(id)
        }
    }

    func enlargeBoundingBox(box: Box) {
        boundingBox = box.merge(boundingBox)
    }
    
    func append(node: RTreeNode) {
        ids.append(node.id)
        nodes.append(node)

        guard let bb = boundingBox else {
            boundingBox = node.boundingBox
            return
        }
        boundingBox = bb.merge(node.boundingBox)
    }
    
    func appendContentsOf(nodes: NodeContainer) {
        for (_, node) in nodes.enumerate() {
            append(node)
        }
    }
    
    func appendContentsOf(nodes: [RTreeNode]) {
        for (_, node) in nodes.enumerate() {
            append(node)
        }
    }

    func removeAtIndex(index: Int) {
        nodes.removeAtIndex(index)
    }
    
    func missingToMinimum() -> Int {
        return params.minEntries - nodes.count
    }
    
    func purge() {
        nodes.removeAll()
        loaded = false
    }
}
