import Foundation

class Params {
    let minEntries: Int // minimum number of entries in a node
    let maxEntries: Int // maximum number of entries that will fit into one node
    let dimensions: UInt

    var splitter: Splitter?
    
    let diskPagesPath: String?
    let keepToDepth: UInt?

    init(min: Int, max: Int, dimensions: UInt) {
        assert(min <= max/2) // suggested in [1]
        minEntries = min
        maxEntries = max

        assert(dimensions >= 2)
        self.dimensions = dimensions
        
        keepToDepth = nil
        diskPagesPath = nil
    }
    
    init(min: Int, max: Int, dimensions: UInt, diskPagesPath: String, keepToDepth: UInt) {
        assert(min <= max/2) // suggested in [1]
        minEntries = min
        maxEntries = max

        assert(dimensions >= 2)
        self.dimensions = dimensions
        
        self.keepToDepth = keepToDepth
        self.diskPagesPath = diskPagesPath
    }
    
    func setSplitter(splitter: Splitter) {
        self.splitter = splitter
    }
    
    func setDefaultSplitter() {
        self.splitter = QuadraticSplitter(params: self)
    }

}
