final class QuadraticSplitter: Splitter {
    let params: Params

    required init(params: Params) {
        self.params = params
    }
    
    func split(nodes: NodeContainer, toInsert: Box) -> (l: NodeContainer, ll: NodeContainer) {
        let (l, ll, rest) = pickSeeds(nodes, toInsert: toInsert)

        while !rest.isEmpty {
            // QS2
            if (l.missingToMinimum() == rest.count) {
                l.appendContentsOf(rest)
                break
            } else if (ll.missingToMinimum() == rest.count) {
                ll.appendContentsOf(rest)
                break
            }
            
            // QS3
            let next = pickNext(l, ll, nodes: rest)
            if next.score > 0 {
                // area of l+next was larger then ll+next
                ll.append(next.node)
            } else if (next.score < 0) {
                l.append(next.node)
            } else {
                resolveTie(l, ll, node: next.node)
            }
            
            rest.removeAtIndex(next.nodeIndex)
        }
        
        return (l, ll)
    }
    
    func resolveTie(l: NodeContainer, _ ll: NodeContainer, node: RTreeNode) {
        // resolve ties by adding the entry to the group with smaller area, then to
        // the one with fewer entries, then to either
        if l.area < ll.area {
            l.append(node)
            return
        } else if l.area > ll.area {
            ll.append(node)
            return
        }
        
        if l.count < ll.count {
            l.append(node)
        } else {
            // this also the "then to either" case
            ll.append(node)
        }
    }
    
    private func pickNext(l: NodeContainer, _ ll: NodeContainer, nodes: NodeContainer) -> (node: RTreeNode, nodeIndex: Int, score: Double) {
        var greatestPreference: (node: RTreeNode, nodeIndex: Int, score: Double)?
        
        for (n, node) in nodes.enumerate() {
            let areaDiffL = Area.compute(node.boundingBox, b: l.boundingBox!) - l.area
            let areaDiffLL = Area.compute(node.boundingBox, b: ll.boundingBox!) - ll.area
            let score = areaDiffL - areaDiffLL
            
            guard let greatest = greatestPreference else {
                greatestPreference = (node, n, score)
                continue
            }
            
            if abs(score) > abs(greatest.score) {
                greatestPreference = (node, n, score)
            }
        }
        
        guard let greatest = greatestPreference else {
            fatalError()
        }
        return greatest
    }
}
