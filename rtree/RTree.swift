import Cocoa

struct RTree {
    var params: Params
    var root: RTreeNode
    
    init(params: Params) {
        self.params = params
        self.params.setDefaultSplitter()
        root = RTreeNode(params: self.params)
    }
    
    mutating func insert(box: Box) {
        if box.dimensions != params.dimensions {
            fatalError("Expected \(params.dimensions), \(box.dimensions) given")
        }

        root = root.insert(box)
    }
    
    func search(range: Box) -> [Box] {
        var rangeMut = range
        var results: [Box] = []
        
//        var _nodesVisited = 0
        
        var stack = List<RTreeNode>.End.cons(root)
        while let node = stack.pop() {
//            _nodesVisited += 1;
            let bb = node.boundingBox;
            if (!bb.intersects(&rangeMut)) {
                continue
            }
            
            if node.value != nil {
                results.append(node.boundingBox)
            }
            
            node.children.loadNodesFromDisk()
            for node in node.children.nodes {
                stack.push(node)
            }
        }
        
//        print("nodes visited: \(_nodesVisited)")

        return results
    }
}
