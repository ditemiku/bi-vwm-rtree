import Cocoa
import Foundation

final class RTreeNode {
    var id: Id
    var params: Params
    var children: NodeContainer
    var value: Box?
    var isLeaf = false
    private var shouldPersist = true // did subtree change since last persist

    /**
     * Creates root
     */
    init(params: Params) {
        self.id = Id()
        self.params = params
        isLeaf = true
        children = NodeContainer(params: params)
    }

    init(params: Params, value: Box) {
        self.id = Id()
        self.params = params
        self.value = value
        self.children = NodeContainer(params: params)
    }

    convenience init(params: Params, nodes: [RTreeNode]) {
        self.init(params: params, nodes: NodeContainer(params: params, nodes: nodes))
    }

    init(params: Params, nodes: NodeContainer) {
        self.id = Id()
        self.params = params
        self.children = nodes
    }
    
    init(params: Params, id: Id, data: NSData) {
        self.id = id
        self.params = params
        self.shouldPersist = false
        
        var offset = sizeof(Bool);
        data.getBytes(&self.isLeaf, range: NSMakeRange(0, offset))

        var hasValue: Bool = false
        data.getBytes(&hasValue, range: NSMakeRange(offset, sizeof(Bool)))
        offset += sizeof(Bool)

        if hasValue {
            var newValue: Box
            (newValue, offset) = Box.fromData(data, offset: offset)
            value = newValue
        }
        
        self.children = NodeContainer(params: params, data: data, offset: offset)
    }

    func invalidate() {
        shouldPersist = true
    }
    
    func persist() {
        if !shouldPersist {
            return
        }

        var hasValue = value != nil;
        
        let data = NSMutableData()
        data.appendBytes(&isLeaf, length: sizeof(Bool))
        data.appendBytes(&hasValue, length: sizeof(Bool))
        
        if value != nil {
            value!.writeToBytes(data)
        }

        children.writeIdsBytes(data)
        
        let pageFile = preparePageFile()
        data.writeToFile(pageFile, atomically: false)

        if children.isLoaded {
            for (_, node) in children.enumerate() {
                node.persist()
            }
        }
    }
    
    var isFull: Bool {
        get {
            return children.count >= params.maxEntries
        }
    }

    var boundingBox: Box {
        get {
            if value != nil {
                return value!
            }
            return children.boundingBox!
        }
    }

    private func split(toInsert: Box) -> (l: NodeContainer, ll: NodeContainer) {
        return params.splitter!.split(children, toInsert: toInsert)
    }

    /**
     * returns new root
     */
    func insert(box: Box) -> RTreeNode {
        let (nodeA, nodeB_) = insert(RTreeNode(params: params, value: box), depth: 0)
        guard let nodeB = nodeB_ else {
            return nodeA
        }
        return RTreeNode(params: params, nodes: [nodeA, nodeB])
        // TODO this is incorrect and probably loses nodes?
        // UPDATE: but split is always just 2 elements so it's probably ok
    }
    
    /**
     * returns old node, or two new nodes
     * when requireLeaf is false entry is inserted immediately into self.children and splitting might occur
     */
    private func insert(entry: RTreeNode, depth: UInt, requireLeaf: Bool = true) -> (nodeA: RTreeNode, nodeB: RTreeNode?) {
        children.loadNodesFromDisk()
        invalidate()

        if (!requireLeaf || isLeaf) {
            if (!isFull) {
                children.append(entry)
                return (self, nil)
            }

            children.append(entry)
            let parts = split(entry.boundingBox)
            let l = RTreeNode(params: params, nodes: parts.l)
            let ll = RTreeNode(params: params, nodes: parts.ll)

            if (isLeaf) {
                l.isLeaf = true
                ll.isLeaf = true
                isLeaf = false
            }

            return (l, ll)
        }

        let (child, childIndex) = chooseChild(entry.boundingBox)
        let (nodeA, _nodeB) = child.insert(entry, depth: depth + 1)
        children.enlargeBoundingBox(entry.boundingBox)
        guard let nodeB = _nodeB else {
            // child did not split, so we didn't split
            return (self, nil)
        }
        
        // child did split, we may split if we are full
        children.removeAtIndex(childIndex)
        children.append(nodeA) // this one cannot split, we just removed nodeIndex
        let result = insert(nodeB, depth: depth, requireLeaf: false) // this one may split
        if params.keepToDepth != nil && depth > params.keepToDepth {
            purge()
        }
        return result
    }
    
    
    // ChooseLeaf, but returns best child (which might not be a leaf)
    private func chooseChild(entryBox: Box) -> (node: RTreeNode, index: Int) {
        var bestCandidate: (node: RTreeNode, index: Int)?
        var bestArea: Double = Double.infinity
        
        for (n, node) in children.enumerate() {
            let thisArea = Area.compute(node.boundingBox, b: entryBox) - node.boundingBox.area

            if (thisArea < bestArea) {
                bestCandidate = (node, n)
                bestArea = thisArea
            }
        }
        
        return bestCandidate! // assume at least one child node exists
    }

//    func search(range: Box, inout results: [Box]) {
//        if (!range.intersects(boundingBox)) {
//            return
//        }
//        
//        if value != nil {
//            results.append(boundingBox)
//        }
//
//        children.loadNodesFromDisk()
//        for node in children.nodes {
//            node.search(range, results: &results)
//        }
//    }
    
    static func getPageFile(id: Id, params: Params) -> (String, String) {
        let dir = "\(params.diskPagesPath!)/\(id.a)/\(id.b)"
        let filename = "\(dir)/\(id.c)"
        return (dir, filename)
    }
    
    private func preparePageFile() -> String {
        let (dir, file) = RTreeNode.getPageFile(id, params: params)

        do {
            try NSFileManager.defaultManager().createDirectoryAtPath(dir, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            fatalError(error.description)
        }

        return file
    }
    
    func purge() {
        persist()
        children.purge()
    }
}
