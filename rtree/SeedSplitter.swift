import Foundation

extension Splitter {
    final func pickSeeds(nodes: NodeContainer, toInsert: Box) -> (l: NodeContainer, ll: NodeContainer, rest: NodeContainer) {
        var worstPair: (a: RTreeNode, b: RTreeNode, indexA: Int, indexB: Int)?
        var worstScore = -Double.infinity
        
        let count = nodes.count
        for n in 0 ..< count {
            for m in n + 1 ..< count {
                let nodeA = nodes[n]
                let nodeB = nodes[m]
                
                let boxAI = nodeA.boundingBox.merge(toInsert)
                let boxBI = nodeB.boundingBox.merge(toInsert)
                
                let score = Area.compute(boxAI, b: boxBI) - boxAI.area - boxBI.area
                if score > worstScore {
                    worstScore = score
                    worstPair = (nodeA, nodeB, n, m)
                }
            }
        }
        
        guard let seeds = worstPair else {
            fatalError()
        }
        
        let rest = nodes;
        rest.removeAtIndex(seeds.indexB) // removing B first because B > A
        rest.removeAtIndex(seeds.indexA)
        
        let l = NodeContainer(params: params)
        l.append(seeds.a)
        let ll = NodeContainer(params: params)
        ll.append(seeds.b)
        return (l, ll, rest)
    }
}
