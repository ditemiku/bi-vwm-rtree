struct Segment {
    let min: Int
    let max: Int
    
    init(min: Int, max: Int) {
        self.min = min
        self.max = max
    }
    
    init(x1: Int, x2: Int) {
        if x1 <= x2 {
            self.init(min: x1, max: x2)
        } else {
            self.init(min: x2, max: x1)
        }
    }
}

func ==(lhs: Segment, rhs: Segment) -> Bool {
    return lhs.min == rhs.min && lhs.max == rhs.max
}

func !=(lhs: Segment, rhs: Segment) -> Bool {
    return !(lhs == rhs)
}
