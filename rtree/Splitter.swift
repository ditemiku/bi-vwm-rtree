protocol Splitter {
    var params: Params {get}

    init(params: Params)
    func split(nodes: NodeContainer, toInsert: Box) -> (l: NodeContainer, ll: NodeContainer)
}
