import Cocoa

extension Box {
    private func _pad(int: Int) -> String {
        return String(format: "%02d", int)
    }
    func _debugText() -> String {
        var text = ""
        for (_, segment) in segments.enumerate() {
            text += "\(_pad(segment.min))-\(_pad(segment.max)) "
        }
        return text
    }
}

// http://stackoverflow.com/a/29263098/326257
extension NSImage {
    var imagePNGRepresentation: NSData {
        return NSBitmapImageRep(data: TIFFRepresentation!)!.representationUsingType(.NSPNGFileType, properties: [:])!
    }
    func savePNG(path:String) -> Bool {
        return imagePNGRepresentation.writeToFile(path, atomically: true)
    }
}

extension RTree {
    func _debug() {
        _debugPrint()
        print("")
        _debugImage().savePNG("/tmp/debug.png")
    }
    
    func _debugPrint() {
        root._debugPrint()
    }
    
    func _debugImage() -> NSImage {
        let size: Int = 500
        let scale: Int = 1
        
        let scaledSize = Int(size * scale)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.PremultipliedLast.rawValue)
        let context = CGBitmapContextCreate(
            nil,
            scaledSize,
            scaledSize,
            8,
            0,
            colorSpace,
            bitmapInfo.rawValue)
        
        root._debugImage(context!, scale: scale, level: 0)
        
        let image = CGBitmapContextCreateImage(context)
        return NSImage(CGImage: image!, size: CGSize(width: scaledSize, height: scaledSize))
    }
}

extension RTreeNode {
    func _debugPrint(depth: Int = 0) {
        let indent = String(count: 2 * depth, repeatedValue: Character(" "))
        let isValue = value != nil ? String("value") : String("")
        let leaf = isLeaf ? String("LEAF") : String("")

        print("\(indent)- [\(boundingBox._debugText())] \(isValue) \(leaf)")
        for (_, node) in children.enumerate() {
            node._debugPrint(depth + 1)
        }
    }
    
    func _debugImage(context: CGContext, scale: Int, level: Int) {
        let colors: [(CGFloat, CGFloat, CGFloat)] = [
            (1.0, 0.0, 0.0),
            (0.0, 0.5, 0.0),
            (0.0, 0.0, 0.5),
            (1.0, 0.0, 1.0),
            (1.0, 1.0, 0.0),
            (0.0, 1.0, 1.0)
        ]
        let color = colors[level % colors.count]
        
        CGContextSetRGBStrokeColor(context, color.0, color.1, color.2, 0.8);
        let width = boundingBox.segments[0].max - boundingBox.segments[0].min
        let height = boundingBox.segments[1].max - boundingBox.segments[1].min
        let rect = CGRectMake(
            CGFloat.init(integerLiteral: Int(scale * boundingBox.segments[0].min + 2 * level)),
            CGFloat.init(integerLiteral: Int(scale * boundingBox.segments[1].min + 2 * level)),
            CGFloat.init(integerLiteral: Int(scale * width - 4 * level)),
            CGFloat.init(integerLiteral: Int(scale * height - 4 * level))
        )
        CGContextStrokeRect(context, rect)
        
        for (_, node) in children.enumerate() {
            node._debugImage(context, scale: scale, level: level + 1)
        }
    }

}
