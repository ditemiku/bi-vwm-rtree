import Foundation
import Cocoa

// [1] http://pages.cs.wisc.edu/~cs764-1/rtree.pdf

var tree = RTree(params: Params(min: 20, max: 50, dimensions: 2))

tree.insert(Box(x1: 10, x2: 20, y1: 10, y2: 20))
tree._debug()

tree.insert(Box(x1: 5, x2: 15, y1: 5, y2: 15))
tree._debug()

tree.insert(Box(x1: 0, x2: 3, y1: 0, y2: 3))
tree._debug()

tree.insert(Box(x1: 3, x2: 6, y1: 0, y2: 3))
tree._debug()

tree.insert(Box(x1: 8, x2: 20, y1: 5, y2: 15))
tree._debug()

srandom(1) // intentionally static salt for repeatable runs

//for _ in Range<Int>(start: 0, end: 30) {
//    tree.insert(Box.initRandom(mod: 50))
//    tree._debug()
//}
//
//for _ in Range<Int>(start: 0, end: 10_000) {
//    tree.insert(Box.initRandom())
//}
//
//for _ in Range<Int32>(start: 0, end: 100_00) {
//    let x = Int32(random() % 50)
//    let y = Int32(random() % 50)
//    let size = Int32(random() % 10)
//    tree.search(Box(x1: x, x2: x + size, y1: y, y2: y + size))
//}

for n in 0...100_000 {
    tree.insert(Box(x1: n, x2: n + 10, y1: n, y2: n + 10))
}

for n in 0...10_000_000 {
    tree.search(Box(x1: n, x2: n + 10, y1: n, y2: n + 10))
}

print("end")
